import axios from "axios";

const api = axios.create({
  baseURL: "https://chatin-vivo.herokuapp.com",
  headers: {
    Authorization:
      "eyJhbGciOiJIUzI1NiJ9.eyJ1c2VyX2lkIjoxfQ.p3EAgO4STzO_zDAqjAji2-2bvBp9nHuYn8g4wX6SPSU",
    "Content-Type": "application/json",
  },
});

const fetchUsers = async () => {
  const res = await api.get("/users");
  return res.data;
};

const fetchConversation = async (id) => {
  const body = {
    conversation: {
      speaker_id: id,
    },
  };

  const res = await api.post("/conversations", body);
  return res.data;
};

const postMessage = async (id, content) => {
  const body = {
    message: {
      conversation_id: id,
      content,
    },
  };

  const res = await api.post("/messages", body);
  return res.data;
};

export default api;
export { fetchUsers, fetchConversation, postMessage };
