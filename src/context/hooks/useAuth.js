import { useState, useEffect } from 'react' 

import api from '../../services/api'

export default function useAuth() {
    const [authenticated, setAuthenticated] = useState(true)
    
    const handleLogin = (history, user) => {
        api.post('/auth/login', user)
        .then((response) => {
            localStorage.setItem('token', JSON.stringify(response.data.token))
            localStorage.setItem('userLogged', JSON.stringify(response.data.user))
            api.defaults.headers.Authorization = response.data.token
            setAuthenticated(true)
            history.push('/chat-home')
        })
        .catch((err) => {
            console.log(err)
        })
    }

    const handleLogout = (history) => {
        localStorage.removeItem('token')
        localStorage.removeItem('userLogged')
        api.defaults.headers.Authorization = undefined
        setAuthenticated(false)
    }

    return { authenticated, handleLogin, handleLogout }
}