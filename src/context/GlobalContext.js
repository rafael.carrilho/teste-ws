import React, { useState } from "react";

import useAuth from './hooks/useAuth'

const GlobalContext = React.createContext();

const GlobalProvider = ({ children }) => {
  const [currentConversation, setCurrentConversation] = useState(null);
  const [user, setUser] = useState({
    name: "Rafael Carrilho",
    email: "rafael@email.com",
    id: 1,
  });

  const closeConversation = () => {
    setCurrentConversation(null);
  };

  const { authenticated, handleLogin, handleLogout } = useAuth()

  return (
    <GlobalContext.Provider
      value={{
        user,
        currentConversation,
        setCurrentConversation,
        closeConversation,
        authenticated,
        handleLogin,
        handleLogout
      }}
    >
      {children}
    </GlobalContext.Provider>
  );
};

export default GlobalContext;
export { GlobalProvider };
