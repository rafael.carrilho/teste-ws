import React, { useContext } from 'react'
import { BrowserRouter as Router, Switch, Redirect, Route } from 'react-router-dom'

import GlobalContext from '../context/GlobalContext'
import Home from '../pages/Home'
import Login from '../pages/Login/index'

function CustomRoute({ isPrivate, ...rest }) {
    const { authenticated } = useContext(GlobalContext)

    if(isPrivate && !authenticated) {
        return <Redirect to='/login' />
    }

    return <Route {...rest}/>
}

function Routes() {

    return (
        <Router>
            <Switch>
                <CustomRoute isPrivate exact path='/chat-home' component={Home}/>
                <CustomRoute exact path='/login' component={Login}/>
                <CustomRoute path='/'><Redirect to='/login' /></CustomRoute>
            </Switch>
        </Router>
    )
}

export default Routes