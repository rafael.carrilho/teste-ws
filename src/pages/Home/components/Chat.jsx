import React, { useContext, useEffect, useRef, useState } from "react";
import ScrollToBottom from "react-scroll-to-bottom";
import ActionCable from "actioncable";
import { postMessage } from "../../../services/api";
import GlobalContext from '../../../context/GlobalContext'

function Chat({ conversation }) {
  const context = useContext(GlobalContext);
  const cable = ActionCable.createConsumer(
    "wss://chatin-vivo.herokuapp.com/cable"
  );

  const [messages, setMessages] = useState(conversation.messages);
  const [input, setInput] = useState("");

  const handleInputChange = (e) => {
    e.target.value && setInput(e.target.value);
  };

  const sendMessage = async (e) => {
    e.preventDefault();
    await postMessage(conversation.id, input);
    setInput("");
  };

  const handleReceivedMessages = (payload) => {
    setMessages(payload);
  };

  const createSubscripton = () => {
    cable.subscriptions.create(
      {
        channel: "ConversationChannel",
        room: "ChatRoom-" + conversation.id,
      },
      {
        received: (payload) => {
          handleReceivedMessages(payload);
        },
      }
    );
  };

  useEffect(() => {
    createSubscripton();
    console.log(conversation)
  }, []);

  return (
    <>
      <div className="chat">
        <button className="btn m-auto" onClick={context.closeConversation}>Fechar chat</button>
        <ScrollToBottom className="messages">
          {messages.map((message, key) => (
            <p key={key} className={`message ${message.sender_id === context.user.id ? "to-right" : "to-left"}`}>
              {message.content}
            </p>
          ))}
        </ScrollToBottom>
        <form onSubmit={sendMessage}>
          <textarea value={input} onChange={handleInputChange} />
          <input className="btn" type="submit" />
        </form>
      </div>
    </>
  );
}

export default Chat;
