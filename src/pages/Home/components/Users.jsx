import React, { useEffect, useState, useContext } from "react";

import GlobalContext from "../../../context/GlobalContext";
import { fetchConversation, fetchUsers } from "../../../services/api";

function Users() {
  const context = useContext(GlobalContext);
  const [users, setUsers] = useState([]);

  const loadUsers = async () => {
    const data = await fetchUsers();
    setUsers(data);
  };

  const openChat = async (id) => {
    const conversation = await fetchConversation(id);
    context.setCurrentConversation(conversation);
  };

  useEffect(() => {
    loadUsers();
  }, []);

  return (
    <div className="users">
      <h2>Your contacts</h2>

      <ul className="users-list">
        {users.map((user, key) => {
          if (user.id !== context.user.id) {
            return (
              <li className="user" key={key}>
                <div>
                  <h3>{user.name}</h3>
                  <h4>{user.email}</h4>
                </div>
                <div>
                  <button
                    className="btn"
                    onClick={() => {
                      openChat(user.id);
                    }}
                  >
                    Open chat
                  </button>
                </div>
              </li>
            );
          }
        })}
      </ul>
    </div>
  );
}

export default Users;
