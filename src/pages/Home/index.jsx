import React, { useContext, useEffect, useState } from "react";
import GlobalContext from "../../context/GlobalContext";

import Users from "./components/Users";
import Chat from "./components/Chat";

import "./style.css";

function Home() {
  const context = useContext(GlobalContext);

  const [conversation, setConversation] = useState(context.currentConversation);

  useEffect(() => {
    setConversation(context.currentConversation);
  }, [context.currentConversation, conversation]);

  return (
    <div id="home" className="container">
      {conversation && <Chat conversation={conversation} />}
      {!conversation && <Users/>}
    </div>
  );
}

export default Home;
