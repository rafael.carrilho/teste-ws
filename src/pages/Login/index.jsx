import React, { useState, useContext} from 'react'
import './style.css'

import { useHistory } from 'react-router-dom'
import GlobalContext from '../../context/GlobalContext'

function Login() {
    const [email, setEmail] = useState("")
    const [password, setPassword] = useState("")

    const history = useHistory()
    const { handleLogin } = useContext(GlobalContext)

    const user = {
        "user": {
            email,
            password
        }
    }

    return (
        <div className="page-wrapper">
            <fieldset>
                <h1>Login</h1>
                <label htmlFor="email">E-mail</label>
                <input onChange={(e) => setEmail(e.target.value)} type="text" name="email" id="0" />
                <label htmlFor="password">Senha</label>
                <input onChange={(e) => setPassword(e.target.value)} type="password" name="password" id="1" />
                <button onClick={() => handleLogin(history, user)} className="btn login">Entrar</button>
            </fieldset>
        </div>
    )
}

export default Login
