import React from "react";
import Home from "./pages/Home";
import { ActionCableProvider } from 'react-actioncable-provider';

import { GlobalProvider } from "./context/GlobalContext";
import "./style/reset.css";
import "./style/global.css";
import Routes from "./routes/routes";

function App() {
  return (
    <ActionCableProvider url="wss://chatin-vivo.herokuapp.com/cable">
      <GlobalProvider>
        <Routes />
      </GlobalProvider>
    </ActionCableProvider>
  );
}

export default App;
